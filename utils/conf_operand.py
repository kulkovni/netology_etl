# модуль для быстрого доступа к переменным локального окружения пользователя
# каждая функция выполняет (в большинсве своем) единсвенную задачу - получить доступ к значению определнной переменной

import environs


env = environs.Env()
env.read_env()


def get_user_email() -> str:
     return env("MY_EMAIL")


def get_kaggle_username() -> str:
     return env("USERNAME")

 
def get_kaggle_pwd() -> str:
     return env("KKEY")


def get_kaggle_ds_name() -> str:
     return env("DATASETNAME")

    
def get_airflow_owner() -> str:
    return env("AIRFLOW_OWNER")


def get_db_conf(host_flag) -> dict:
     """ Return the conf of db

     :param host_flag: dwh or any another value
     :return: dict of conf
     """
     return dict(host=env("HOST_DB"),
                 database=env(
                      f"DBNAME_{'DWH' if host_flag=='dwh' else 'LAKE'}"),
                 port=env("PORT_DB"),
                 user=env("USER_DB"),
                 password=env("PASSWORD_DB"))
     
 
def get_conf_param(param: str) -> str:
     """ return confedentional cred or parametr

     :param param: string name ofr parametr
     :return: string of value
     """
     return env(param)


if __name__ == "__main__":
    print(get_db_conf('dwh'))
    