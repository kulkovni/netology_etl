from pathlib import Path
from datetime import datetime, timedelta
from typing import Any
from airflow.utils.dates import days_ago


def create_default_args(owner, **kwargs) -> dict[str, Any]:
    """Вспомогательная функция для генерации аргументов DAG по умолчанию

    :param owner: УЗ пользователя, под которым будет запущена задача
    :return: словарь для работы с DAG
    """
    default_args = {
        "owner": owner,
        "start_date": days_ago(1),
        "execution_timeout": timedelta(minutes=10),
        "run_as_owner": True
    }
    
    return {**default_args, **kwargs}



if __name__ == "__main__":
    for i in create_default_args(owner="kulkovni").items():
        print(i)