import logging
import sys
from typing import Any
import pandas as pd
from pathlib import Path
import psycopg2 as ps2
import sqlalchemy
from pandas import DataFrame

sys.path.insert(0, f"{Path.home()}/netology_etl")
from utils.conf_operand import get_db_conf


class My_db_methods:
    """
    Class for work my DBs

    """
    def __init__(self, flag) -> None:
        """ Init DB-operator on `flag` -> or DWH Data Base, or Lake DB

        :param flag: `dwh` or any another value 
        """
        self.conf_db = get_db_conf(flag)
        self.db_alias = "postgresql"
        self.flag = flag

    def get_cursor_and_conn_db(self) -> callable:
        """
        Return cursos callable object to Data Base of DWH

        :param flag: dwh or any another value
            :return: cursor object
        """
        conn = ps2.connect(
            host=self.conf_db["host"],
            port=self.conf_db["port"],
            database=self.conf_db["database"],
            user=self.conf_db["user"],
            password=self.conf_db["password"]
            )
        
        cursor = conn.cursor()
        return dict(cursor=cursor, conn=conn)
            
    def _get_db_engine(self) -> callable:
        """
        Инициализирует engine для работы с БД
        """
        user, pwd = self.conf_db['user'], self.conf_db['password']
        host, port, db = self.conf_db['host'], self.conf_db['port'], self.conf_db['database']
        
        conf_str = f"{self.db_alias}://{user}:{pwd}@{host}:{port}/{db}"
        engine = sqlalchemy.create_engine(conf_str) 
        return engine

    def push_df_to_db(self, df: DataFrame,
                            table_name: str, 
                            schema: str, 
                            if_exists: str = "replace") -> None:
        """
        Запишет в postgre pd.DataFrame, передынный в df

        :param df: DataFrame под запись
        :param table_name: имя новой таблицы в БД
        """
        
        df.to_sql(name=table_name, 
                  schema=schema, 
                  con=self._get_db_engine(),
                  if_exists=if_exists,
                  index=False,
                  method="multi")
    
    def get_allData_fromDB_to_pd(self, 
                                schema: str,
                                table_name: str) -> DataFrame:
        """ Простой оператор select всех данных, для быстрого доступа
        к определнным в аргументах схеме и таблице

        :param schema: schema в БД
        :param table_name: таблица в БД
        :return: pd.DataFrame считанный из БД
        """
        
        conn = self.get_cursor_and_conn_db()["conn"]
        df = pd.read_sql_query(sql=f"SELECT * FROM {schema}.{table_name}", 
                               con=conn)
        conn.close()
        return df
    
    def push_query_to_db(self, query: str = None) -> None:
        """ Метод для отправки произвольных запросов в БД
        :param query: тело запроса, defaults to None
        """
        
        cc = self.get_cursor_and_conn_db()
        
        cc["cursor"].execute(query)
        cc["conn"].commit()
        cc["conn"].close()


if __name__ == "__main__":
    test_obj = My_db_methods("lake")
    
    test_obj.push_query_to_db(query="CREATE TABLE test.test_table (id VARCHAR (3));")
    
    
