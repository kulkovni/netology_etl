import os
from pathlib import Path

PATH = f"{Path.home()}/storage_tmp"


def tmp_dir_worker(mode="cre") -> str:
    """ Create / delete dir for tmp data

    :param mode: method to works, defaults to "cre"
    :return: String of tmp path for data
    
    if mode-parametr is incroect - imperative recurtion
    """
    try:
        if mode == "cre":
            os.system(f"mkdir {PATH}")
        elif mode == "del":
            os.system(f"rm -r {PATH}")
        else:
            raise TypeError
    except TypeError:
        print(TypeError + "\n" + \
              f"Script will be is restarting. Your tmp directory will be creating with name as `{PATH}`")
        tmp_dir_worker(mode="cre")
    except Exception:
        print("file_name is exist")
    finally: 
        return PATH
    
def file_renamer(path_to_file: str, final_name: str) -> str:
    """ Renaming file facture

    :param path_to_file: path to without file name
    :return: string of abs_path
    """
    file_name_raw = os.listdir(path_to_file)[0]
    for_cmd = f"{Path.home()}/storage_tmp/'{file_name_raw}'"
    
    path_to_file = f"{path_to_file}/{final_name}"
    os.system(f"mv {for_cmd} {path_to_file}")
    
    return path_to_file
