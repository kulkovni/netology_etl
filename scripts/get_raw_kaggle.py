import kaggle
import logging
import pandas as pd

from utils.db_operand import My_db_methods
from utils.storage_operand import tmp_dir_worker, file_renamer
from utils.conf_operand import get_kaggle_ds_name, get_conf_param

DATASETNAME = get_kaggle_ds_name()


def get_kaggle_data(dataSet: str = DATASETNAME) -> None:
    """ 
    Загрузит фактуру данных с внешнего ресурса с помощь открытого API Kaggle
    данные будут сохранены во временную директорию, 
    имя которого передается в `path`

    :param dataSet: имя dataset с внешнего ресусра kaggle.com, defaults to DATASETNAME
    :param path: путь временной директории для фактуры из kaggle.com, defaults to PATH_SAVE
    """
    # при запуске в Airflow - нужен именно рабочий католог и он == /users/{name_user}/{project_name}:
    # path = f"{Path.cwd()}/storage/"
    
    path = tmp_dir_worker(mode="cre")
    
    kaggle.api.authenticate()
    kaggle.api.dataset_download_files(dataSet, path=path, unzip=True)
    logging.info(f"Data is download to path: {path}") # проблема с логгером. не выводит на веб морде



def push_kaggle_to_db(path_to_file: str) -> None:
    """
    Write or rewrite  DataFrame of raw-kaggle data to Postgre (Lake-LvL)
    :param path_to_file: path to tmp-data
    """
    schema = get_conf_param("SCHEMA")
    table_name = get_conf_param("RAW_KAGGLE")
    
    # т.к. из контекста прилетает только путь директории, но не сам файл
    # нужно предобработать содержимое дирекотрии: 
    path_to_file = file_renamer(path_to_file, "facture.csv")
    
    df = pd.read_csv(path_to_file, sep=",", header=0)
    My_db_methods("lake").push_df_to_db(df=df, 
                                      table_name=table_name, 
                                      schema=schema)
    
if __name__ == "__main__":
    get_kaggle_data()