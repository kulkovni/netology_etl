import sys
from pathlib import Path
import pandas as pd
from pandas import DataFrame

sys.path.insert(1, f"{Path.home()}/netology_etl")

from utils.conf_operand import get_conf_param
from utils.db_operand import My_db_methods
from . import BRANCHES, FACTURE, PRODUCT_LINES, CUSTOMER_TYPES, GENDERS, PAYMENTS


"""
Для справки - почему делаю "топорно" (накидыаю доп атрибут, вместо присовения некоторым атрибутом 
ограничения первичного ключа и декмопзиции без доп атрибутов) :
а) Это проще (simple is better than complex(c - Zen Python))
b) Вместимость. (Если верить документации: объекты postgre типа serial вясет 4 байта, 
    объекты varchar/character могут достигать большего объема 
    + вопрос об автоинкременте, если справочники будут расширяться со временем)
"""


def date_formater(df: DataFrame) -> DataFrame:
    """ Функция для приведения даты к общему формату и сведению в один атрибут 

    :param df: pd.DataFrame
    :return: pd.DataFrame
    """
    df["date_unformat"] = pd.to_datetime(df.date)

    df = df.drop(columns=["date"])
    df = df.rename(columns = {"date_unformat": "date"})
    
    df["time"] = df["time"].replace(to_replace=r'$', value=':00', regex=True)
    
    df["datetime_"] = df["date"].astype(str) + "T" + df["time"]
    df["event_date"] = pd.to_datetime(df.datetime_)
    
    return df.drop(columns=["date", "time", "datetime_"])


def divide_data() -> dict[DataFrame]:
    """ Divide one relationship into 
    decompositioned data objects
    
    That for writing on postgre

    :return: dict of datas
    """
    raw_df = My_db_methods("lake").get_allData_fromDB_to_pd(schema= get_conf_param("SCHEMA"),
                                                        table_name=get_conf_param("RAW_KAGGLE"))
    
    headers = ['invoice_id', 'branch', 'city', 'customer_type',
    'gender', 'product_line', 'unit_price', 'quantity', 'tax_5_percent',
    'total', 'date', 'time', 'payment', 'cogs', 'gross_margine_perc',
    'gross_income','rating']
    
    raw_df.columns = headers
    
    result_df = {
        BRANCHES: raw_df[["branch", "city"]].sort_values("branch").drop_duplicates(),
        FACTURE: date_formater(raw_df.drop(columns=["gross_margine_perc", "gross_income"])),
        CUSTOMER_TYPES: raw_df[["customer_type"]].sort_values("customer_type").drop_duplicates(),
        GENDERS: raw_df[["gender"]].sort_values("gender").drop_duplicates(),
        PRODUCT_LINES: raw_df[["product_line"]].sort_values("product_line").drop_duplicates(),
        PAYMENTS: raw_df[["payment"]].sort_values("payment").drop_duplicates()
    }
    return result_df
    

def write_decompos_table(table_name: str) -> None:
    """ Запишет декомпозированные данные в postgre 

    :param table_name: имя таблицы, которая будет записана
    """
    df = divide_data()[table_name]
    obj = My_db_methods("dwh")
    obj.push_df_to_db(df=df, schema=obj.flag, table_name=table_name)


def altering_constraints(query_list: list[str]) -> None:
    """ Добавит атрибут первичного ключа в каждое из декомпозированных отношений

    :param query_list:
    :return: None
    """
    for query in query_list:
        My_db_methods("dwh").push_query_to_db(query=query)
        
    