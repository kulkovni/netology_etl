import sys
from pathlib import Path
import pandas as pd

sys.path.insert(0, f"{Path.home()}/netology_etl")

from utils.conf_operand import get_conf_param
from utils.db_operand import My_db_methods


def create_metric_csv(query: str, file_name: str, **kwargs) -> None:
    """Соберет определнные sql-запросом метрики 
        и положит в общую папку основного хоста

    :param query: SQL-запрос
    :param file_name: имя файла для записи
    """
    conn = My_db_methods("dwh").get_cursor_and_conn_db()["conn"]
    
    df = pd.read_sql(sql=query, con=conn)
    
    formater = kwargs["data_format"]
    if formater is not None:
        df = df.astype({formater["atribute"]: formater["type"]})
    
    save_path = f"{get_conf_param('MACDIR')}/{file_name}.csv"
    df.to_csv(path_or_buf=save_path, 
              sep=";", header=True, 
              mode="w", date_format="yyyy-mm-dd",
              decimal=".", index=False)


if __name__ == "__main__":
    query = """
            select sf.branches_id, b.city, sum(sf.quantity) as sum_quantity_sails
            from dwh.shop_facts sf
            inner join dwh.branches b on sf.branches_id = b.branches_id
            group by sf.branches_id, b.city
            order by sum_quantity_sails desc;
        """
    create_metric_csv(query=query,
                      file_name="relat_branches",
                      data_format={"atribute": "sum_quantity_sails", "type": int})
    