import sys
from pathlib import Path
from datetime import datetime
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.common.sql.sensors.sql import SqlSensor

sys.path.insert(0, f"{Path.home()}/netology_etl")

from utils.conf_operand import get_conf_param
from utils.db_operand import My_db_methods
from utils.airflow_operand import create_default_args
from scripts.decompos_data import write_decompos_table, altering_constraints
from scripts import BRANCHES, FACTURE, CUSTOMER_TYPES, GENDERS, PRODUCT_LINES, PAYMENTS, SHOP_FACTS

OWNER=get_conf_param("AIRFLOW_OWNER")
DEFAULT_ARGS = create_default_args(owner=OWNER,
                                   start_date=datetime(2023, 5, 9))

DAG_ARGUMENTS = {
    "dag_id": f"{Path(__file__).stem}",
    "tags": ["decomposition", "normalizations"],
    "default_args": DEFAULT_ARGS,
    "schedule_interval": "10 9 */1 * *",
    "max_active_runs": 1,
    
}

SCHEMA =  get_conf_param("SCHEMA_D")
DB_CONN_OBJECT = My_db_methods(SCHEMA)

# For sensor sql:
DB_LAKE = "netology_lake_db"
QUERY_SENSOR=f"SELECT * FROM {get_conf_param('SCHEMA')}.{get_conf_param('RAW_KAGGLE')} LIMIT 1;"

# Query`s:
QUERY_LIST_PK =[f"ALTER TABLE {SCHEMA}.{table} ADD COLUMN {table}_id SERIAL PRIMARY KEY" \
        for table in [BRANCHES, FACTURE, CUSTOMER_TYPES, GENDERS, PRODUCT_LINES, PAYMENTS]
]

QUERY_LIST_FK = [f"""
                 ALTER TABLE {SCHEMA}.{SHOP_FACTS} DROP CONSTRAINT IF EXISTS fk_{table};
                 ALTER TABLE {SCHEMA}.{SHOP_FACTS} ADD CONSTRAINT fk_{table}
                 FOREIGN KEY ({table}_id) REFERENCES {SCHEMA}.{table} ({table}_id)
                 ON DELETE CASCADE;""" \
        for table in [BRANCHES, CUSTOMER_TYPES, GENDERS, PRODUCT_LINES, PAYMENTS]
        ]

JOIN_DATA = f"""CREATE TABLE {SCHEMA}.{SHOP_FACTS} as (
                SELECT f.invoice_id, b.branches_id, pl.product_lines_id, g.gender_id, ct.customer_types_id, 
                py.payment_id,
                f.event_date, f.unit_price, f.quantity, f.total, f.cogs, f.rating, f.tax_5_percent
                FROM dwh.facture as f
                INNER JOIN {SCHEMA}.{BRANCHES} b ON f.branch = b.branch
                INNER JOIN {SCHEMA}.{CUSTOMER_TYPES} ct ON f.customer_type = ct.customer_type
                INNER JOIN {SCHEMA}.{GENDERS} g ON f.gender = g.gender
                INNER JOIN {SCHEMA}.{PRODUCT_LINES} pl ON f.product_line = pl.product_line
                INNER JOIN {SCHEMA}.{PAYMENTS} py ON f.payment = py.payment);"""
                

with DAG(**DAG_ARGUMENTS) as dag:
    drop_that_cascade = PythonOperator(
        task_id="drop_that_cascade",
        python_callable=DB_CONN_OBJECT.push_query_to_db,
        op_kwargs={"query":f"DROP TABLE IF EXISTS {SCHEMA}.shop_facts CASCADE;"},
        run_as_user=OWNER
    )
    # сенсор для проверки загруженной факутуры из kaggle:
    facture_sensor = SqlSensor(
        task_id="facture_sensor",
        poke_interval=10,
        timeout=50,
        soft_fail=False,
        retries=3,
        conn_id=DB_LAKE,
        sql=QUERY_SENSOR,
    )
    
    inserting_facture_data = PythonOperator(
        task_id="inserting_facture_data",
        python_callable=write_decompos_table,
        op_kwargs={"table_name": FACTURE},
        run_as_user=OWNER
    )
    
    inserting_branches_data = PythonOperator(
        task_id="inserting_branches_data",
        python_callable=write_decompos_table,
        op_kwargs={"table_name": BRANCHES},
        run_as_user=OWNER
    )
    
    inserting_customer_type_data = PythonOperator(
        task_id="inserting_customer_type_data",
        python_callable=write_decompos_table,
        op_kwargs={"table_name":CUSTOMER_TYPES},
        run_as_user=OWNER
    )
    
    inserting_genders_data = PythonOperator(
        task_id="inserting_genders_data",
        python_callable=write_decompos_table,
        op_kwargs={"table_name":GENDERS},
        run_as_user=OWNER
    )
    
    inserting_product_line_data = PythonOperator(
        task_id="inserting_product_line_data",
         python_callable=write_decompos_table,
        op_kwargs={"table_name": PRODUCT_LINES},
        run_as_user=OWNER
    )
    
    inserting_payment_data = PythonOperator(
        task_id="inserting_payment_data",
        python_callable=write_decompos_table,
        op_kwargs={"table_name": PAYMENTS},
        run_as_user=OWNER
    )
    
    alter_constraint_pk_step = PythonOperator(
        task_id="alter_constraint_pk_step",
        python_callable=altering_constraints,
        op_kwargs={"query_list": QUERY_LIST_PK},
        run_as_user=OWNER
    )
    
    joinig_data_to_central_start_object = PythonOperator(
        task_id="joinig_data_to_central_start_object",
        python_callable=DB_CONN_OBJECT.push_query_to_db,
        op_kwargs={"query": JOIN_DATA},
        run_as_user=OWNER
    )
    
    drop_facture = PythonOperator(
        task_id="drop_facture",
        python_callable=DB_CONN_OBJECT.push_query_to_db,
        op_kwargs={"query": f"DROP TABLE IF EXISTS {SCHEMA}.{FACTURE} CASCADE;"},
        run_as_user=OWNER
    )
    
    alter_constraint_fk_step = PythonOperator(
        task_id="alter_constraint_fk_step",
        python_callable=altering_constraints,
        op_kwargs={"query_list": QUERY_LIST_FK},
        run_as_user=OWNER
    )
     
    drop_that_cascade \
        >> facture_sensor \
        >> inserting_facture_data \
        >> [inserting_branches_data, inserting_customer_type_data,
            inserting_genders_data, inserting_product_line_data, inserting_payment_data] \
        >> alter_constraint_pk_step  \
        >> joinig_data_to_central_start_object  \
        >> [drop_facture, alter_constraint_fk_step]
        
    
    

