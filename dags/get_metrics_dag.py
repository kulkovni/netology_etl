import sys
from pathlib import Path
from datetime import datetime
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.common.sql.sensors.sql import SqlSensor

sys.path.insert(0, f"{Path.home()}/netology_etl")

from utils.conf_operand import get_conf_param
from utils.airflow_operand import create_default_args
from scripts.examples_of_metrics import create_metric_csv

OWNER=get_conf_param("AIRFLOW_OWNER")
DEFAULT_ARGS = create_default_args(owner=OWNER,
                                   start_date=datetime(2023, 5, 9))

SCHEMA = get_conf_param('SCHEMA_D')
TABLE = "shop_facts"

DAG_ARGUMENTS = {
    "dag_id": f"{Path(__file__).stem}",
    "tags": ["metrics"],
    "default_args": DEFAULT_ARGS,
    "schedule_interval": "0 15 */1 * *",
    "max_active_runs": 1,  
}

BRANCH_QUERY = """ select sf.branches_id, b.city, sum(sf.quantity) as sum_quantity_sails
            from dwh.shop_facts sf
            inner join dwh.branches b on sf.branches_id = b.branches_id
            group by sf.branches_id, b.city
            order by sum_quantity_sails desc;
        """

PAYMENT_QUERY = """ select p.payment, count(sf.payment_id) from dwh.shop_facts sf
            inner join dwh.payment p on p.payment_id = sf.payment_id
            group by p.payment;
        """

PROFIT_QUERY = """select round(sum(sf.cogs::numeric), 2) date_sum_profit, sf.event_date::date
from dwh.shop_facts sf
group by sf.event_date::date
order by sf.event_date::date;"""

AVG_QUERY = """select round(avg(sf.cogs::numeric),2) cnt_avg
from dwh.shop_facts sf;
"""


with DAG(**DAG_ARGUMENTS) as dag:
    sensor = SqlSensor(
        task_id="facture_sensor",
        poke_interval=10,
        timeout=20,
        soft_fail=False,
        retries=2,
        conn_id="netology_dwh_db",
        sql=f"SELECT * FROM {SCHEMA}.{TABLE} LIMIT 1;",)
    
    branches_relat = PythonOperator(
        task_id="branches_relat",
        python_callable=create_metric_csv,
        op_kwargs={"query": BRANCH_QUERY,
                   "file_name": "branch_metric",
                   "data_format":
                       {"atribute":"sum_quantity_sails", "type":int}},
        run_as_user=OWNER
    )
    
    payment_relat = PythonOperator(
        task_id="payment_relat",
        python_callable=create_metric_csv,
        op_kwargs={"query": PAYMENT_QUERY,
                   "file_name": "payment_metric",
                   "data_format": None},
        run_as_user=OWNER
    )
    
    profit_relat = PythonOperator(
        task_id="profit_relat",
        python_callable=create_metric_csv,
        op_kwargs={"query": PROFIT_QUERY,
                    "file_name": "profit_metric",
                   "data_format": None},
        run_as_user=OWNER
    )
    
    avg_relat = PythonOperator(
        task_id="avg_relat",
        python_callable=create_metric_csv,
        op_kwargs={"query": AVG_QUERY,
                    "file_name": "avg_metric",
                   "data_format": None},
        run_as_user=OWNER
    )
    
    
    sensor >> [branches_relat, payment_relat, profit_relat, avg_relat]
    
    