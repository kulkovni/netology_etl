import sys
from pathlib import Path

from airflow import DAG
from airflow.utils.timezone import datetime
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator

sys.path.insert(1, f"{Path.home()}/netology_etl")

from utils.storage_operand import tmp_dir_worker, PATH
from utils.airflow_operand import create_default_args
from utils.conf_operand import get_conf_param

from scripts.get_raw_kaggle import get_kaggle_data, push_kaggle_to_db

OWNER=get_conf_param("AIRFLOW_OWNER")
DEFAULT_ARGS = create_default_args(owner=OWNER,
                                   start_date=datetime(2023, 5, 9))

DAG_ARGUMENTS = {
    "dag_id": f"{Path(__file__).stem}",
    "tags": ["kaggle"],
    "default_args": DEFAULT_ARGS,
    "schedule_interval": "0 9 */1 * *",
    "max_active_runs": 1,
}

DB_LAKE = "netology_lake_db"
TABLE_NAME = f"{get_conf_param('SCHEMA')}.{get_conf_param('RAW_KAGGLE')}"
QUERY_DROP=f"DROP TABLE IF EXISTS {TABLE_NAME};"


with DAG(**DAG_ARGUMENTS) as dag:
    postgre_droped_old_table= PostgresOperator(
        task_id="postgre_droped_old_table",
        postgres_conn_id=DB_LAKE,
        sql=QUERY_DROP
    )
    
    get_data_from_kaggle = PythonOperator(
        task_id="get_data_from_kaggle",
        python_callable=get_kaggle_data,
        run_as_user=OWNER
    )
    
    push_kaggle_data_to_db = PythonOperator(
        task_id="push_kaggle_data_to_db",
        python_callable=push_kaggle_to_db,
        op_kwargs={"path_to_file": PATH}
    )
    
    delete_tmp_dir = PythonOperator(
        task_id="delete_tmp_dir",
        python_callable=tmp_dir_worker,
        op_kwargs={"mode":"del"}
    )

    
    postgre_droped_old_table >> get_data_from_kaggle  >> push_kaggle_data_to_db >> delete_tmp_dir
    